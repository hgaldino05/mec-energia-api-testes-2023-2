import pytest
from tariff_util import response_tariffs_of_distributor

class Tariff:
    def __init__(self, peak_tusd_in_reais_per_kw, peak_tusd_in_reais_per_mwh, peak_te_in_reais_per_mwh, off_peak_tusd_in_reais_per_kw, off_peak_tusd_in_reais_per_mwh, off_peak_te_in_reais_per_mwh, na_tusd_in_reais_per_kw=None):
        self.peak_tusd_in_reais_per_kw = peak_tusd_in_reais_per_kw
        self.peak_tusd_in_reais_per_mwh = peak_tusd_in_reais_per_mwh
        self.peak_te_in_reais_per_mwh = peak_te_in_reais_per_mwh
        self.off_peak_tusd_in_reais_per_kw = off_peak_tusd_in_reais_per_kw
        self.off_peak_tusd_in_reais_per_mwh = off_peak_tusd_in_reais_per_mwh
        self.off_peak_te_in_reais_per_mwh = off_peak_te_in_reais_per_mwh
        self.na_tusd_in_reais_per_kw = na_tusd_in_reais_per_kw

def test_response_tariffs_of_distributor():
    blue_tariff = Tariff(1, 2, 3, 4, 5, 6)
    green_tariff = Tariff(1, 2, 3, 4, 5, 6, 7)
    start_date = "2023-01-01"
    end_date = "2023-12-31"
    pending = False

    response = response_tariffs_of_distributor(start_date, end_date, pending, blue_tariff, green_tariff)

    assert response["start_date"] == start_date
    assert response["end_date"] == end_date
    assert response["overdue"] == pending
    assert response["blue"]["peakTusdInReaisPerKw"] == blue_tariff.peak_tusd_in_reais_per_kw
    assert response["blue"]["peakTusdInReaisPerMwh"] == blue_tariff.peak_tusd_in_reais_per_mwh
    assert response["blue"]["peakTeInReaisPerMwh"] == blue_tariff.peak_te_in_reais_per_mwh
    assert response["blue"]["offPeakTusdInReaisPerKw"] == blue_tariff.off_peak_tusd_in_reais_per_kw
    assert response["blue"]["offPeakTusdInReaisPerMwh"] == blue_tariff.off_peak_tusd_in_reais_per_mwh
    assert response["blue"]["offPeakTeInReaisPerMwh"] == blue_tariff.off_peak_te_in_reais_per_mwh
    assert response["green"]["peakTusdInReaisPerMwh"] == green_tariff.peak_tusd_in_reais_per_mwh
    assert response["green"]["peakTeInReaisPerMwh"] == green_tariff.peak_te_in_reais_per_mwh
    assert response["green"]["offPeakTusdInReaisPerMwh"] == green_tariff.off_peak_tusd_in_reais_per_mwh
    assert response["green"]["offPeakTeInReaisPerMwh"] == green_tariff.off_peak_te_in_reais_per_mwh
    assert response["green"]["naTusdInReaisPerKw"] == green_tariff.na_tusd_in_reais_per_kw

def test_response_tariffs_of_distributor_no_tariffs():
    blue_tariff = None
    green_tariff = None
    start_date = "2023-01-01"
    end_date = "2023-12-31"
    pending = False

    response = response_tariffs_of_distributor(start_date, end_date, pending, blue_tariff, green_tariff)

    assert response["start_date"] == start_date
    assert response["end_date"] == end_date
    assert response["overdue"] == pending
    assert response["blue"] == None
    assert response["green"] == None